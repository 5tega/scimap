import Invention from "../models/Invention";

class InventionService {
    public async getAll(): Promise<any> {
        return await Invention.findAll();
    }

    public async getOne(id: number): Promise<any> {
        const inv = await Invention.findOne({ where: { id } });
        if (inv == null) throw "Invention with given ID doesn't exist";
        return inv;
    }

    public async create(invPayload: any): Promise<any> {
        const inv = await Invention.create(invPayload);
        return inv;
    }

    public async delete(id: number): Promise<any> {
        await Invention.destroy({ where: { id } });
    }

    public async update(invPayload: any): Promise<any> {
        const inv = await Invention.update(invPayload, { where: { id: invPayload.id } });
        return inv;
    }
}

export default new InventionService;