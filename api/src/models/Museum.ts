import db from "./database";
import { Model, STRING, INTEGER, DOUBLE, BOOLEAN } from "sequelize";
import Scientist from "./Scientist";

class Museum extends Model {
  public id!: number;
  public name!: string;
  public location!: string;
  public lat!: number; // Latitude on map
  public lng!: number; // Longitude on map
  public openingDate!: number; // Unix epoch seconds
  public description!: string;
  // Associated scientist fields:
  public showcasedScientistId!: number;
  public showcase!: boolean;
  public showcaseStartDate!: number;
  public showcaseEndDate!: number;
  public image!: string;
}

Museum.init(
  {
    id: {
      type: INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: STRING,
      allowNull: false,
    },
    location: {
        type: STRING,
        allowNull: false
    },
    lat: {
      type: DOUBLE,
      allowNull: false
    },
    lng: {
      type: DOUBLE,
      allowNull: false
    },
    openingDate: {
        type: INTEGER,
        allowNull: false
    },
    description: {
        type: STRING,
        allowNull: true
    },
    showcasedScientistId: {
        type: INTEGER,
        allowNull: true
    },
    showcase: {
      type: BOOLEAN,
      allowNull: false
    },
    showcaseStartDate: {
        type: INTEGER,
        allowNull: true
    },
    showcaseEndDate: {
        type: INTEGER,
        allowNull: true
    },
    image: {
      type: STRING,
      allowNull: true
    }
  },
  {
    timestamps: false,
    sequelize: db,
    tableName: "museums",
  }
);

Museum.belongsTo(Scientist, { as: "showcasedScientist", foreignKey: "showcasedScientistId" });
Scientist.hasOne(Museum, { as: "museum", foreignKey: "showcasedScientistId" });

export default Museum;